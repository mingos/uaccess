<?php
namespace Mingos\uAccess;

/**
 * Test the Identity class
 */
class IdentityTest extends \PHPUnit_Framework_TestCase
{
	/**
	 * @var Rbac
	 */
	private $rbac;

	/**
	 * @var IdentityInterface
	 */
	private $identity;

	/**
	 * @var Factory
	 */
	private $factory;

	public function setUp()
	{
		$this->factory = new Factory();

		$this->rbac = $this->factory->rbac();
		$this->identity = $this->factory->identity();

		$this->rbac->addRole("king_arthur");
		$this->rbac->addRole("sir_robin");
		$this->rbac->addRole("sir_lancelot");
		$this->rbac->addRole("sir_galahad");
		$this->rbac->addRole("sir_bedevere");
		$this->rbac->addRole("minstrel");

		$this->rbac->getRole("king_arthur")->getPermissions()->grant("lob_hand_grenade");
		$this->rbac->getRole("sir_robin")->getPermissions()->grant("run_away");
		$this->rbac->getRole("minstrel")->getPermissions()->grant("sing");

		$this->rbac->getRole("king_arthur")
			->addSubordinate($this->rbac->getRole("sir_robin"))
			->addSubordinate($this->rbac->getRole("sir_lancelot"))
			->addSubordinate($this->rbac->getRole("sir_galahad"))
			->addSubordinate($this->rbac->getRole("sir_bedevere"));

		$this->rbac->getRole("sir_robin")
			->addSubordinate($this->rbac->getRole("minstrel"));
	}

	/**
	 * The identity should correctly add/set roles and check their existence
	 */
	public function testRoleAdding()
	{
		$this->identity->addRole($this->rbac->getRole("sir_lancelot"));
		$this->identity->addRole($this->rbac->getRole("sir_bedevere"));

		$this->assertTrue($this->identity->hasRole("sir_lancelot"));
		$this->assertTrue($this->identity->hasRole("sir_bedevere"));

		$this->identity->setRoles([
			$this->rbac->getRole("sir_galahad"),
			$this->rbac->getRole("sir_robin")
		]);

		$this->assertFalse($this->identity->hasRole("sir_lancelot"));
		$this->assertFalse($this->identity->hasRole("sir_bedevere"));
		$this->assertTrue($this->identity->hasRole("sir_galahad"));
		$this->assertTrue($this->identity->hasRole("sir_robin"));
	}

	/**
	 * The identity should return its roles
	 */
	public function testRoleGetting()
	{
		$this->identity->setRoles([
			$this->rbac->getRole("sir_galahad"),
			$this->rbac->getRole("sir_robin")
		]);

		$roles = $this->identity->getRoles();
		$this->assertCount(2, $roles);
		$this->assertArrayHasKey("sir_galahad", $roles);
		$this->assertSame($this->rbac->getRole("sir_galahad"), $roles["sir_galahad"]);
		$this->assertArrayHasKey("sir_robin", $roles);
		$this->assertSame($this->rbac->getRole("sir_robin"), $roles["sir_robin"]);
	}

	/**
	 * The identity should correctly check role permissions, taking into account all its roles.
	 */
	public function testRolePermissionsChecking()
	{
		$this->identity->setRoles([
			$this->rbac->getRole("sir_robin")
		]);

		$this->assertTrue($this->identity->isGranted("sing"));
		$this->assertTrue($this->identity->isGranted("run_away"));
		$this->assertFalse($this->identity->isGranted("lob_hand_grenade"));
		$this->assertFalse($this->identity->isGranted("find_holy_grail"));

		$this->identity->setRoles([
			$this->rbac->getRole("king_arthur")
		]);

		$this->assertTrue($this->identity->isGranted("sing"));
		$this->assertTrue($this->identity->isGranted("run_away"));
		$this->assertTrue($this->identity->isGranted("lob_hand_grenade"));
		$this->assertFalse($this->identity->isGranted("find_holy_grail"));

		$this->identity->setRoles([
			$this->rbac->getRole("sir_lancelot")
		]);

		$this->assertFalse($this->identity->isGranted("sing"));
		$this->assertFalse($this->identity->isGranted("run_away"));
		$this->assertFalse($this->identity->isGranted("lob_hand_grenade"));
		$this->assertFalse($this->identity->isGranted("find_holy_grail"));
	}

	/**
	 * ACL permissions should take precedence before RBAC permissions
	 */
	public function testAcl()
	{
		$this->identity->setRoles([
			$this->rbac->getRole("sir_robin")
		]);

		$this->identity->getPermissions()->grant("lob_hand_grenade");
		$this->identity->getPermissions()->deny("run_away");

		$this->assertTrue($this->identity->isGranted("lob_hand_grenade"));
		$this->assertFalse($this->identity->isGranted("run_away"));
	}

	/**
	 * An identity should be able to return a serialised version of itself
	 */
	public function testSerialisation()
	{
		$this->identity->setRoles([
			$this->rbac->getRole("sir_robin"),
			$this->rbac->getRole("sir_lancelot")
		]);

		$this->identity->getPermissions()->set([
			"wield_the_excalibur" => false,
			"find_the_holy_grail" => true
		]);

		$serialised = $this->identity->serialise();

		$this->assertArrayHasKey("roles", $serialised);
		$this->assertTrue(in_array("sir_robin", $serialised["roles"]));
		$this->assertTrue(in_array("sir_lancelot", $serialised["roles"]));
		$this->assertArrayHasKey("permissions", $serialised);
	}

	/**
	 * The identity should be hydratable
	 */
	public function testHydration()
	{
		$serialised = [
			"roles" => [
				"sir_robin",
				"sir_lancelot"
			],
			"permissions" => [
				"wield_the_excalibur" => false,
				"find_the_holy_grail" => true
			]
		];

		$this->identity->hydrate($serialised, $this->rbac);

		$this->assertTrue($this->identity->hasRole("sir_robin"));
		$this->assertTrue($this->identity->hasRole("sir_lancelot"));
		$this->assertTrue($this->identity->getPermissions()->has("wield_the_excalibur"));
		$this->assertTrue($this->identity->getPermissions()->has("find_the_holy_grail"));
	}
}
