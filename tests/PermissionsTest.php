<?php
namespace Mingos\uAccess;

/**
 * Test the Permissions class
 */
class PermissionsTest extends \PHPUnit_Framework_TestCase
{
	/**
	 * @var Factory
	 */
	private $factory;

	public function setUp()
	{
		$this->factory = new Factory();
	}

	/**
	 * Test whether granting, denying and clearing permissions works as expected.
	 */
	public function testPermissionManagement()
	{
		$permissions = $this->factory->permissions();

		$permissions->grant("run_away");
		$permissions->deny("fight_the_three_headed_knight");

		$refl = new \ReflectionProperty($permissions, "permissions");
		$refl->setAccessible(true);
		$permissionsProperty = $refl->getValue($permissions);

		$this->assertTrue(is_array($permissionsProperty));
		$this->assertCount(2, $permissionsProperty);
		$this->assertArrayHasKey("run_away", $permissionsProperty);
		$this->assertTrue($permissionsProperty["run_away"]);
		$this->assertArrayHasKey("fight_the_three_headed_knight", $permissionsProperty);
		$this->assertFalse($permissionsProperty["fight_the_three_headed_knight"]);
		$this->assertArrayNotHasKey("find_the_holy_grail", $permissionsProperty);

		$this->assertTrue($permissions->get("run_away"));
		$this->assertFalse($permissions->get("fight_the_three_headed_knight"));
		$this->assertFalse($permissions->get("find_the_holy_grail"));
	}

	/**
	 * the AbstractPermissions::hasPermission should return correct values
	 */
	public function testPermissionOwnership()
	{
		$permissions = $this->factory->permissions();

		$permissions->grant("run_away");
		$permissions->deny("fight_the_three_headed_knight");

		$this->assertTrue($permissions->has("run_away"));
		$this->assertTrue($permissions->has("fight_the_three_headed_knight"));
		$this->assertFalse($permissions->has("find_the_holy_grail"));
	}
}
