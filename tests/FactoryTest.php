<?php
namespace Mingos\uAccess;

/**
 * Test the Factory class
 */
class FactoryTest extends \PHPUnit_Framework_TestCase
{
	/**
	 * @var Factory
	 */
	private $factory;

	public function setUp()
	{
		$this->factory = new Factory();
	}

	/**
	 * The factory should create Identity objects
	 */
	public function testIdentity()
	{
		$this->assertInstanceOf('Mingos\uAccess\Identity', $this->factory->identity());
	}

	/**
	 * The factory should create Permissions objects
	 */
	public function testPermissions()
	{
		$this->assertInstanceOf('Mingos\uAccess\Permissions', $this->factory->permissions());
	}

	/**
	 * The factory should create Rbac objects
	 */
	public function testRbac()
	{
		$this->assertInstanceOf('Mingos\uAccess\Rbac', $this->factory->rbac());
	}

	/**
	 * The factory should create Role objects
	 */
	public function testRole()
	{
		$this->assertInstanceOf('Mingos\uAccess\Role', $this->factory->role("lumberjack"));
	}
}
