<?php
namespace Mingos\uAccess;

/**
 * Test the Rbac class
 */
class RbacTest extends \PHPUnit_Framework_TestCase
{
	/**
	 * @var Rbac
	 */
	private $rbac;

	/**
	 * @var Factory
	 */
	private $factory;

	public function setUp()
	{
		$this->factory = new Factory();
		$this->rbac = $this->factory->rbac();
	}

	/**
	 * Test adding roles
	 */
	public function testRoleAdding()
	{
		$this->rbac->addRole("sir_robin");
		$this->rbac->addRole($this->factory->role("sir_lancelot"));

		$refl = new \ReflectionProperty($this->rbac, "roles");
		$refl->setAccessible(true);
		$roles = $refl->getValue($this->rbac);

		$this->assertCount(2, $roles);
		$this->assertArrayHasKey("sir_robin", $roles);
		$this->assertArrayHasKey("sir_lancelot", $roles);
		$this->assertInstanceOf('Mingos\uAccess\Role', $roles["sir_robin"]);
		$this->assertInstanceOf('Mingos\uAccess\Role', $roles["sir_lancelot"]);
	}

	/**
	 * The default behaviour is not to overwrite roles, but it should be possible to force the opposite behaviour.
	 */
	public function testRoleOverwriting()
	{
		$role = $this->factory->role("sir_robin");
		$this->rbac->addRole($role);

		// new role, but the name clashes
		$this->rbac->addRole("sir_robin");

		$refl = new \ReflectionProperty($this->rbac, "roles");
		$refl->setAccessible(true);

		$roles = $refl->getValue($this->rbac);
		$this->assertSame($role, $roles["sir_robin"]);

		// new role, force overwrite
		$this->rbac->addRole("sir_robin", true);

		$roles = $refl->getValue($this->rbac);
		$this->assertNotSame($role, $roles["sir_robin"]);
	}

	/**
	 * Test whether it's possible to check for the existence of a given role.
	 */
	public function testRoleExistence()
	{
		$this->assertFalse($this->rbac->hasRole("king_arthur"));

		$this->rbac->addRole("king_arthur");

		$this->assertTrue($this->rbac->hasRole("king_arthur"));
	}

	/**
	 * It must be possible to fetch a role by name. Inexistent roles should yield an exception.
	 */
	public function testRoleFetching()
	{
		$role = $this->factory->role("king_arthur");
		$this->rbac->addRole($role);

		$this->assertSame($role, $this->rbac->getRole("king_arthur"));

		$this->setExpectedException("Exception");
		$this->rbac->getRole("bridgekeeper");
	}

	/**
	 * The Rbac should return an array of serialised roles.
	 */
	public function testSerialisation()
	{
		$this->rbac->addRole("king_arthur");
		$this->rbac->addRole("sir_robin");

		// let's make sure the serialise() method is called
		$mock = \Mockery::mock('Mingos\uAccess\Role');
		$mock->shouldReceive("getName")->twice()->andReturn("minstrel");
		$mock->shouldReceive("serialise")->once()->andReturn("Bravely bold Sir Robin rode forth from Camelot!");
		$this->rbac->addRole($mock);

		$serialised = $this->rbac->serialise();

		$this->assertCount(3, $serialised);
		$this->assertArrayHasKey("king_arthur", $serialised);
		$this->assertArrayHasKey("sir_robin", $serialised);
		$this->assertArrayHasKey("minstrel", $serialised);
		$this->assertEquals("Bravely bold Sir Robin rode forth from Camelot!", $serialised["minstrel"]);
	}

	/**
	 * The Rbac should enable populating it with an array or serialised roles
	 */
	public function testHydration()
	{
		$this->rbac->hydrate([
			"king_arthur" => [
				"permissions" => [],
				"subordinates" => []
			],
			"sir_robin" => [
				"permissions" => [],
				"subordinates" => []
			]
		]);

		$this->assertTrue($this->rbac->hasRole("king_arthur"));
		$this->assertTrue($this->rbac->hasRole("sir_robin"));
	}

	public function tearDown()
	{
		\Mockery::close();
	}
}
