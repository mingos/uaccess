<?php
namespace Mingos\uAccess;

/**
 * Test the Role class
 */
class RoleTest extends \PHPUnit_Framework_TestCase
{
	/**
	 * @var Factory
	 */
	private $factory;

	public function setUp()
	{
		$this->factory = new Factory();
		$this->rbac = $this->factory->rbac();
	}

	/**
	 * Role name getter test
	 */
	public function testRoleName()
	{
		$role = $this->factory->role("sir_robin");
		$this->assertEquals("sir_robin", $role->getName());
	}

	/**
	 * Test whether it's possible to safely set superior and subordinate roles on each other
	 */
	public function testSuperiorsAndSubordinates()
	{
		$base = $this->factory->role("sir_robin");
		$subordinate = $this->factory->role("minstrel");
		$superior = $this->factory->role("king_arthur");

		$base->addSuperior($superior);
		$base->addSubordinate($subordinate);

		$reflSub = new \ReflectionProperty($base, "subordinates");
		$reflSup = new \ReflectionProperty($base, "superiors");
		$reflSub->setAccessible(true);
		$reflSup->setAccessible(true);

		$baseSup = $reflSup->getValue($base);
		$baseSub = $reflSub->getValue($base);

		$superiorSub = $reflSub->getValue($superior);
		$subordinateSup = $reflSup->getValue($subordinate);

		$this->assertTrue(is_array($baseSub));
		$this->assertTrue(is_array($baseSup));

		$this->assertArrayHasKey("minstrel", $baseSub);
		$this->assertArrayHasKey("king_arthur", $baseSup);
		$this->assertSame($subordinate, $baseSub["minstrel"]);
		$this->assertSame($superior, $baseSup["king_arthur"]);

		$this->assertArrayHasKey("sir_robin", $superiorSub);
		$this->assertArrayHasKey("sir_robin", $subordinateSup);
	}

	/**
	 * Superior roles inherit all privileges from their subordinate roles, unless they have the permission in
	 * question set directly (i.e. overridden). Permission that is not set is equivalent to denied.
	 */
	public function testPermissionInheritance()
	{
		$arthur = $this->factory->role("king_arthur");
		$robin = $this->factory->role("sir_robin");
		$minstrel = $this->factory->role("minstrel");

		$arthur->addSubordinate($robin);
		$robin->addSubordinate($minstrel);

		$minstrel->getPermissions()->grant("sing");
		$robin->getPermissions()->grant("run_away");
		$robin->getPermissions()->deny("sing");
		$arthur->getPermissions()->grant("lob_hand_grenade");

		$this->assertTrue($arthur->isGranted("lob_hand_grenade"));
		$this->assertTrue($arthur->isGranted("run_away"));
		$this->assertFalse($arthur->isGranted("sing"));

		$this->assertFalse($robin->isGranted("lob_hand_grenade"));
		$this->assertTrue($robin->isGranted("run_away"));
		$this->assertFalse($robin->isGranted("sing"));

		$this->assertFalse($minstrel->isGranted("lob_hand_grenade"));
		$this->assertFalse($minstrel->isGranted("run_away"));
		$this->assertTrue($minstrel->isGranted("sing"));
	}

	/**
	 * A circular dependency in role hierarchy, if encountered, should yield an exception.
	 */
	public function testCircularRoleDependency()
	{
		$rock = $this->factory->role("rock");
		$paper = $this->factory->role("paper");
		$scissors = $this->factory->role("scissors");

		$rock->addSubordinate($scissors);
		$scissors->addSubordinate($paper);
		$paper->addSubordinate($rock);

		$this->setExpectedException("Exception");

		$rock->isGranted("win");
	}

	/**
	 * A role should allow serialisation to array
	 */
	public function testSerialisation()
	{
		$role = $this->factory->role("king_arthur");
		$subordinate = $this->factory->role("sir_robin");

		$role->addSubordinate($subordinate);
		$role->getPermissions()
			->grant("lob_hand_grenade")
			->deny("chicken_out");

		$expected = [
			"permissions" => [
				"lob_hand_grenade" => true,
				"chicken_out" => false
			],
			"subordinates" => [
				"sir_robin"
			]
		];
		$this->assertEquals(serialize($expected), serialize($role->serialise()));
	}

	/**
	 * A role should allow to be hydrated from an array
	 */
	public function testHydration()
	{
		$rbac = $this->factory->rbac();
		$role = $this->factory->role("king_arthur");
		$rbac->addRole($role);

		$role->hydrate([
			"permissions" => [
				"lob_hand_grenade" => true,
				"chicken_out" => false
			],
			"subordinates" => [
				"sir_robin"
			]
		], $rbac);

		$this->assertTrue($role->isGranted("lob_hand_grenade"));

		// give Sir Robin a permission and check whether King Arthur receives it
		$rbac->getRole("sir_robin")->getPermissions()->grant("confront_the_bridgekeeper");
		$this->assertTrue($role->isGranted("confront_the_bridgekeeper"));
	}

	/**
	 * A role should be able to find out whether another role is its subordinate or not,
	 * both direct and indirect.
	 */
	public function testSubordinateChecking()
	{
		$grandfather = $this->factory->role("grandfather");
		$father = $this->factory->role("father");
		$uncle = $this->factory->role("uncle");
		$son = $this->factory->role("son");

		$grandfather
			->addSubordinate($father)
			->addSubordinate($uncle);

		$father
			->addSubordinate($son);

		$this->assertTrue($grandfather->hasSubordinate($father));
		$this->assertTrue($grandfather->hasSubordinate($father, true));
		$this->assertTrue($grandfather->hasSubordinate($uncle));
		$this->assertTrue($grandfather->hasSubordinate($uncle, true));
		$this->assertTrue($grandfather->hasSubordinate($son));
		$this->assertFalse($grandfather->hasSubordinate($son, true));

		$this->assertFalse($father->hasSubordinate($grandfather));
		$this->assertFalse($father->hasSubordinate($grandfather, true));
		$this->assertFalse($father->hasSubordinate($uncle));
		$this->assertFalse($father->hasSubordinate($uncle, true));
		$this->assertTrue($father->hasSubordinate($son));
		$this->assertTrue($father->hasSubordinate($son, true));

		$this->assertFalse($uncle->hasSubordinate($grandfather));
		$this->assertFalse($uncle->hasSubordinate($grandfather, true));
		$this->assertFalse($uncle->hasSubordinate($father));
		$this->assertFalse($uncle->hasSubordinate($father, true));
		$this->assertFalse($uncle->hasSubordinate($son));
		$this->assertFalse($uncle->hasSubordinate($son, true));

		$this->assertFalse($son->hasSubordinate($grandfather));
		$this->assertFalse($son->hasSubordinate($grandfather, true));
		$this->assertFalse($son->hasSubordinate($father));
		$this->assertFalse($son->hasSubordinate($father, true));
		$this->assertFalse($son->hasSubordinate($uncle));
		$this->assertFalse($son->hasSubordinate($uncle, true));

		$this->assertTrue($grandfather->hasSubordinate("father"));
		$this->assertTrue($grandfather->hasSubordinate("father", true));
		$this->assertTrue($grandfather->hasSubordinate("uncle"));
		$this->assertTrue($grandfather->hasSubordinate("uncle", true));
		$this->assertTrue($grandfather->hasSubordinate("son"));
		$this->assertFalse($grandfather->hasSubordinate("son", true));

		$this->assertFalse($father->hasSubordinate("grandfather"));
		$this->assertFalse($father->hasSubordinate("grandfather", true));
		$this->assertFalse($father->hasSubordinate("uncle"));
		$this->assertFalse($father->hasSubordinate("uncle", true));
		$this->assertTrue($father->hasSubordinate("son"));
		$this->assertTrue($father->hasSubordinate("son", true));

		$this->assertFalse($uncle->hasSubordinate("grandfather"));
		$this->assertFalse($uncle->hasSubordinate("grandfather", true));
		$this->assertFalse($uncle->hasSubordinate("father"));
		$this->assertFalse($uncle->hasSubordinate("father", true));
		$this->assertFalse($uncle->hasSubordinate("son"));
		$this->assertFalse($uncle->hasSubordinate("son", true));

		$this->assertFalse($son->hasSubordinate("grandfather"));
		$this->assertFalse($son->hasSubordinate("grandfather", true));
		$this->assertFalse($son->hasSubordinate("father"));
		$this->assertFalse($son->hasSubordinate("father", true));
		$this->assertFalse($son->hasSubordinate("uncle"));
		$this->assertFalse($son->hasSubordinate("uncle", true));
	}

	/**
	 * A role should be able to find out whether another role is its superior or not,
	 * both direct and indirect.
	 */
	public function testSuperiorChecking()
	{
		$grandfather = $this->factory->role("grandfather");
		$father = $this->factory->role("father");
		$uncle = $this->factory->role("uncle");
		$son = $this->factory->role("son");

		$grandfather
			->addSubordinate($father)
			->addSubordinate($uncle);

		$father
			->addSubordinate($son);

		$this->assertFalse($grandfather->hasSuperior($father));
		$this->assertFalse($grandfather->hasSuperior($father, true));
		$this->assertFalse($grandfather->hasSuperior($uncle));
		$this->assertFalse($grandfather->hasSuperior($uncle, true));
		$this->assertFalse($grandfather->hasSuperior($son));
		$this->assertFalse($grandfather->hasSuperior($son, true));

		$this->assertTrue($father->hasSuperior($grandfather));
		$this->assertTrue($father->hasSuperior($grandfather, true));
		$this->assertFalse($father->hasSuperior($uncle));
		$this->assertFalse($father->hasSuperior($uncle, true));
		$this->assertFalse($father->hasSuperior($son));
		$this->assertFalse($father->hasSuperior($son, true));

		$this->assertTrue($uncle->hasSuperior($grandfather));
		$this->assertTrue($uncle->hasSuperior($grandfather, true));
		$this->assertFalse($uncle->hasSuperior($father));
		$this->assertFalse($uncle->hasSuperior($father, true));
		$this->assertFalse($uncle->hasSuperior($son));
		$this->assertFalse($uncle->hasSuperior($son, true));

		$this->assertTrue($son->hasSuperior($grandfather));
		$this->assertFalse($son->hasSuperior($grandfather, true));
		$this->assertTrue($son->hasSuperior($father));
		$this->assertTrue($son->hasSuperior($father, true));
		$this->assertFalse($son->hasSuperior($uncle));
		$this->assertFalse($son->hasSuperior($uncle, true));

		$this->assertFalse($grandfather->hasSuperior("father"));
		$this->assertFalse($grandfather->hasSuperior("father", true));
		$this->assertFalse($grandfather->hasSuperior("uncle"));
		$this->assertFalse($grandfather->hasSuperior("uncle", true));
		$this->assertFalse($grandfather->hasSuperior("son"));
		$this->assertFalse($grandfather->hasSuperior("son", true));

		$this->assertTrue($father->hasSuperior("grandfather"));
		$this->assertTrue($father->hasSuperior("grandfather", true));
		$this->assertFalse($father->hasSuperior("uncle"));
		$this->assertFalse($father->hasSuperior("uncle", true));
		$this->assertFalse($father->hasSuperior("son"));
		$this->assertFalse($father->hasSuperior("son", true));

		$this->assertTrue($uncle->hasSuperior("grandfather"));
		$this->assertTrue($uncle->hasSuperior("grandfather", true));
		$this->assertFalse($uncle->hasSuperior("father"));
		$this->assertFalse($uncle->hasSuperior("father", true));
		$this->assertFalse($uncle->hasSuperior("son"));
		$this->assertFalse($uncle->hasSuperior("son", true));

		$this->assertTrue($son->hasSuperior("grandfather"));
		$this->assertFalse($son->hasSuperior("grandfather", true));
		$this->assertTrue($son->hasSuperior("father"));
		$this->assertTrue($son->hasSuperior("father", true));
		$this->assertFalse($son->hasSuperior("uncle"));
		$this->assertFalse($son->hasSuperior("uncle", true));
	}
}
