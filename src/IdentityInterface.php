<?php
namespace Mingos\uAccess;

/**
 * An identity interface
 */
interface IdentityInterface
{
	/**
	 * Assign a new role to the identity
	 *
	 * @param  RoleInterface     $role Role
	 * @return IdentityInterface       Provides a fluent interface
	 */
	public function addRole(RoleInterface $role);

	/**
	 * Set all roles assigned to the identity
	 *
	 * @param  array             $newRoles Role names
	 * @return IdentityInterface           Provides a fluent interface
	 */
	public function setRoles(array $newRoles);

	/**
	 * Retrieve all roles assigned to the identity
	 *
	 * @return array
	 */
	public function getRoles();

	/**
	 * Check if the identity has a given role
	 *
	 * @param  string  $role Role name
	 * @return boolean
	 */
	public function hasRole($role);

	/**
	 * Check if the identity is granted a permission
	 *
	 * @param  string  $permission Permission name
	 * @return boolean
	 */
	public function isGranted($permission);

	/**
	 * Fetch the identity permissions
	 *
	 * @return PermissionsInterface
	 */
	public function getPermissions();

	/**
	 * Populate the identity with data
	 *
	 * @param  array $input Data array as returned by IdentityInterface::serialise.
	 * @param  Rbac  $rbac  The Rbac object to retrieve roles from
	 * @return self         Provides a fluent interface
	 */
	public function hydrate(array $input, Rbac $rbac);

	/**
	 * Return an array representation of the identity
	 *
	 * @return array Array with the followinf structure:
	 *               array(
	 *                   "permissions" => array(
	 *                       "{permissionName}" => boolean,
	 *                       ...
	 *                    ),
	 *                    "roles" => array(
	 *                        "{roleName}",
	 *                        ...
	 *                    )
	 *               )
	 */
	public function serialise();
}
