<?php
namespace Mingos\uAccess;

/**
 * Library version
 */
class Version
{
	const VERSION = "1.6.0";
}
