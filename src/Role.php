<?php
namespace Mingos\uAccess;

/**
 * A role
 */
class Role implements RoleInterface
{
	/**
	 * Role name
	 * @var string
	 */
	private $name;

	/**
	 * Superior roles
	 * @var RoleInterface[]
	 */
	private $superiors = [];

	/**
	 * Subordinate roles
	 * @var RoleInterface[]
	 */
	private $subordinates = [];

	/**
	 * Role permissions
	 * @var PermissionsInterface
	 */
	protected $permissions;

	/**
	 * Constructor
	 *
	 * @param string               $name        Role name
	 * @param PermissionsInterface $permissions A Permissions instance
	 */
	public function __construct($name, PermissionsInterface $permissions)
	{
		$this->name = $name;
		$this->permissions = $permissions;
	}

	/**
	 * @inheritdoc
	 */
	public function getName()
	{
		return $this->name;
	}

	/**
	 * @inheritdoc
	 */
	public function isGranted($permission)
	{
		return $this->_isGranted($permission);
	}

	/**
	 * Determine whether a role is granted a given permission or inherits access.
	 *
	 * @param  string             $permission Permission name
	 * @param  RoleInterface|null $requester  The role that first requested the permission check (used to prevent
	 *                                        circular dependencies)
	 * @return boolean
	 * @throws \Exception                     When a circular dependency is detected
	 */
	private function _isGranted($permission, $requester = null)
	{
		if ($this === $requester) {
			throw new \Exception("Circular role dependency detected.");
		}

		if (null === $requester) {
			$requester = $this;
		}

		if ($this->permissions->has($permission)) {
			$result = $this->permissions->get($permission);
		} else {
			$result = false;
			/**
			 * @var Role $subordinate
			 */
			foreach ($this->subordinates as $subordinate) {
				$result = $result || $subordinate->_isGranted($permission, $requester);
			}
		}

		return $result;
	}

	/**
	 * @inheritdoc
	 */
	public function addSuperior(RoleInterface $role)
	{
		if (!array_key_exists($role->getName(), $this->superiors)) {
			$this->superiors[$role->getName()] = $role;
			$role->addSubordinate($this);
		}

		return $this;
	}

	/**
	 * @inheritdoc
	 */
	public function addSubordinate(RoleInterface $role)
	{
		if (!array_key_exists($role->getName(), $this->subordinates)) {
			$this->subordinates[$role->getName()] = $role;
			$role->addSuperior($this);
		}

		return $this;
	}

	/**
	 * @inheritdoc
	 */
	public function hasSubordinate($role, $direct = false)
	{
		$role = ($role instanceof RoleInterface ? $role->getName() : $role);

		if (array_key_exists($role, $this->subordinates)) {
			return true;
		}

		if (!$direct) {
			/**
			 * @var RoleInterface $subordinate
			 */
			foreach ($this->subordinates as $subordinate) {
				if ($subordinate->hasSubordinate($role)) {
					return true;
				}
			}
		}

		return false;
	}

	/**
	 * @inheritdoc
	 */
	public function hasSuperior($role, $direct = false)
	{
		$role = ($role instanceof RoleInterface ? $role->getName() : $role);

		if (array_key_exists($role, $this->superiors)) {
			return true;
		}

		if (!$direct) {
			/**
			 * @var RoleInterface $superior
			 */
			foreach ($this->superiors as $superior) {
				if ($superior->hasSuperior($role)) {
					return true;
				}
			}
		}

		return false;
	}

	/**
	 * @inheritdoc
	 */
	public function getPermissions()
	{
		return $this->permissions;
	}

	/**
	 * @inheritdoc
	 */
	public function hydrate(array $input, Rbac $rbac)
	{
		// add permissions
		$this->permissions->set($input["permissions"]);

		// add subordinate roles
		foreach ($input["subordinates"] as $subordinate) {
			$rbac->addRole($subordinate);
			$this->addSubordinate($rbac->getRole($subordinate));
		}

		return $this;
	}

	/**
	 * @inheritdoc
	 */
	public function serialise()
	{
		$output = array(
			"permissions" => $this->permissions->get(),
			"subordinates" => []
		);

		/**
		 * @var RoleInterface $subordinate
		 */
		foreach ($this->subordinates as $subordinate) {
			$output["subordinates"][] = $subordinate->getName();
		}

		return $output;
	}
}
