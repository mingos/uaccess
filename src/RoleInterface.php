<?php
namespace Mingos\uAccess;

/**
 * A role interface
 */
interface RoleInterface
{
	/**
	 * Get the role name
	 *
	 * @return string
	 */
	public function getName();

	/**
	 * Check if the role or any of its subordinate roles has a given permission.
	 *
	 * @param  string  $permission Permission name
	 * @return boolean
	 */
	public function isGranted($permission);

	/**
	 * Add a superior role
	 *
	 * @param  RoleInterface $role Superior role
	 * @return RoleInterface       Provides a fluent interface
	 */
	public function addSuperior(RoleInterface $role);

	/**
	 * Add a subordinate role
	 *
	 * @param  RoleInterface $role Subordinate role
	 * @return RoleInterface       Provides a fluent interface
	 */
	public function addSubordinate(RoleInterface $role);

	/**
	 * Check if the role has a given subordinate (if the role inherits from a subordinate)
	 *
	 * @param  string|RoleInterface $role   The role that will be sought
	 * @param  boolean              $direct Whether only direct subordinates should be checked
	 * @return boolean
	 */
	public function hasSubordinate($role, $direct = false);

	/**
	 * Check if the role has a given superior (if the role is inherited by a superior)
	 *
	 * @param  string|RoleInterface $role   The role that will be sought
	 * @param  boolean              $direct Whether only direct superiors should be checked
	 * @return boolean
	 */
	public function hasSuperior($role, $direct = false);

	/**
	 * Fetch the role permissions
	 *
	 * @return PermissionsInterface
	 */
	public function getPermissions();

	/**
	 * Populate the role with data
	 *
	 * @param  array $input Data array as returned by RoleInterface::serialise.
	 * @param  Rbac  $rbac  The Rbac object to retrieve the subordinate roles from
	 * @return self         Provides a fluent interface
	 */
	public function hydrate(array $input, Rbac $rbac);

	/**
	 * Return an array representation of the role
	 *
	 * @return array Array with the followinf structure:
	 *               array(
	 *                   "permissions" => array(
	 *                       "{permissionName}" => boolean,
	 *                       ...
	 *                    ),
	 *                    "subordinates" => array(
	 *                        "{roleName}",
	 *                        ...
	 *                    )
	 *               )
	 */
	public function serialise();
}
