<?php
namespace Mingos\uAccess;

/**
 * A permissions interface
 */
interface PermissionsInterface
{
	/**
	 * Grant a permission
	 *
	 * @param  string        $permission Permission name
	 * @return RoleInterface             Provides a fluent interface
	 */
	public function grant($permission);

	/**
	 * Deny a permission
	 *
	 * @param  string        $permission Permission name
	 * @return RoleInterface             Provides a fluent interface
	 */
	public function deny($permission);

	/**
	 * Set the entire permissions array
	 *
	 * @param  array                $permissions Permissions array
	 * @return PermissionsInterface              Provides a fluent interface
	 */
	public function set(array $permissions);

	/**
	 * Check if a given permission is defined (reggardless whether it's granted or denied)
	 *
	 * @param  string  $permission Permission name
	 * @return boolean
	 */
	public function has($permission);

	/**
	 * Get a permission's value or all permissions
	 *
	 * @param  string       permission Permission name
	 * @return boolean|null
	 */
	public function get($permission = null);
}
