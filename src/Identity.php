<?php
namespace Mingos\uAccess;

/**
 * An identity.
 */
class Identity implements IdentityInterface
{
	/**
	 * Array of roles assigned to the identity
	 * @var array
	 */
	protected $roles = [];

	/**
	 * Identity permissions
	 * @var PermissionsInterface
	 */
	protected $permissions;

	/**
	 * Constructor
	 *
	 * @param PermissionsInterface $permissions A Permissions instance
	 */
	public function __construct(PermissionsInterface $permissions)
	{
		$this->permissions = $permissions;
	}

	/**
	 * @inheritdoc
	 */
	public function addRole(RoleInterface $role)
	{
		if (!in_array($role, $this->roles)) {
			$this->roles[$role->getName()] = $role;
		}

		return $this;
	}

	/**
	 * @inheritdoc
	 */
	public function setRoles(array $roles)
	{
		$this->roles = [];
		foreach ($roles as $role) {
			$this->addRole($role);
		}

		return $this;
	}

	/**
	 * @inheritdoc
	 */
	public function getRoles()
	{
		return $this->roles;
	}

	/**
	 * @inheritdoc
	 */
	public function hasRole($role)
	{
		return array_key_exists($role, $this->roles);
	}

	/**
	 * @inheritdoc
	 */
	public function isGranted($permission)
	{
		// ACL
		if ($this->permissions->has($permission)) {
			return $this->permissions->get($permission);
		}

		// RBAC
		/**
		 * @var Role $role
		 */
		foreach ($this->roles as $role) {
			if ($role->isGranted($permission)) {
				return true;
			}
		}

		return false;
	}

	/**
	 * @inheritdoc
	 */
	public function getPermissions()
	{
		return $this->permissions;
	}

	/**
	 * @inheritdoc
	 */
	public function hydrate(array $input, Rbac $rbac)
	{
		// add ACL permissions
		$this->permissions->set($input["permissions"]);

		// add RBAC roles
		foreach ($input["roles"] as $role) {
			$rbac->addRole($role);
			$this->addRole($rbac->getRole($role));
		}

		return $this;
	}

	/**
	 * @inheritdoc
	 */
	public function serialise()
	{
		$output = [
			"permissions" => $this->permissions->get(),
			"roles" => []
		];

		/**
		 * @var RoleInterface $role
		 */
		foreach ($this->roles as $role) {
			$output["roles"][] = $role->getName();
		}

		return $output;
	}
}
