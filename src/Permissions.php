<?php
namespace Mingos\uAccess;

/**
 * An abstract class implementing the permissions interface
 */
class Permissions implements PermissionsInterface
{
	/**
	 * Permissions keyed array: (string) key => (boolean) granted
	 * @var array
	 */
	protected $permissions = [];

	/**
	 * @inheritdoc
	 */
	public function grant($permission)
	{
		$this->permissions[$permission] = true;

		return $this;
	}

	/**
	 * @inheritdoc
	 */
	public function deny($permission)
	{
		$this->permissions[$permission] = false;

		return $this;
	}

	/**
	 * @inheritdoc
	 */
	public function set(array $permissions)
	{
		$this->permissions = $permissions;

		return $this;
	}

	/**
	 * @inheritdoc
	 */
	public function has($permission)
	{
		return array_key_exists($permission, $this->permissions);
	}

	/**
	 * @inheritdoc
	 */
	public function get($permission = null)
	{
		if (null === $permission) {
			return $this->permissions;
		}

		if ($this->has($permission)) {
			return $this->permissions[$permission];
		}

		return false;
	}
}
