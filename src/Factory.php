<?php
namespace Mingos\uAccess;

/**
 * Factory for creating uAccess-specific object instances
 */
class Factory
{
	/**
	 * Instantiate an Identity object
	 *
	 * @return Identity
	 */
	public function identity()
	{
		return new Identity($this->permissions());
	}

	/**
	 * Instantiate a Permissions object
	 *
	 * @return Permissions
	 */
	public function permissions()
	{
		return new Permissions();
	}

	/**
	 * Instantiate a Rbac object
	 *
	 * @return Rbac
	 */
	public function rbac()
	{
		return new Rbac();
	}

	/**
	 * Instantiate a Role object
	 *
	 * @param  string $roleName Name of the role
	 * @return Role
	 */
	public function role($roleName)
	{
		return new Role($roleName, $this->permissions());
	}
}
