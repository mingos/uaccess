<?php
namespace Mingos\uAccess;

/**
 * Role-based access control implementation
 */
class Rbac
{
	/**
	 * Defined roles
	 * @var array
	 */
	private $roles = [];

	/**
	 * Add a new role
	 *
	 * @param  RoleInterface|string $role      The role or the role name.
	 * @param  boolean              $overwrite [optional] Flag indicating whether duplicated role names should result in
	 *                                         replacing the existent role with a new one. Defaults to false.
	 * @return Rbac                            Provides a fluent interface
	 */
	public function addRole($role, $overwrite = false)
	{
		// get role name
		$roleName = ($role instanceof RoleInterface) ? $role->getName() : $role;

		// if role overwriting is diabled, check for overwrite
		if (!$overwrite && $this->hasRole($roleName)) {
			return $this;
		}

		// add role
		if ($role instanceof RoleInterface) {
			$this->roles[$roleName] = $role;
		} else {
			$this->roles[$roleName] = new Role($roleName, new Permissions());
		}

		return $this;
	}

	/**
	 * Check if a role exists
	 *
	 * @param  string  $roleName Name of the role
	 * @return boolean
	 */
	public function hasRole($roleName)
	{
		return array_key_exists($roleName, $this->roles);
	}

	/**
	 * Fetch a role by its name.
	 *
	 * @param  string        $roleName Name of the role to fetch
	 * @return RoleInterface
	 * @throws \Exception              When an inexistent role has been requested
	 */
	public function getRole($roleName)
	{
		if (array_key_exists($roleName, $this->roles)) {
			return $this->roles[$roleName];
		}

		throw new \Exception("Role {$roleName} has not been created.");
	}

	/**
	 * Hydrate the Rbac with complete roles and permissions from an input array (e.g. cached)
	 *
	 * @param  array $input Array of roles with their subordinate roles and permissions, following the exact format of
	 *                      the Rbac::serialise() method.
	 * @return self         Provides a fluent interface
	 */
	public function hydrate($input)
	{
		foreach ($input as $roleName => $role) {
			$this->addRole($roleName);
			$this->getRole($roleName)->hydrate($role, $this);
		}

		return $this;
	}

	/**
	 * Serialise the contents of the object to an array
	 *
	 * @return array Serialised object with the following structure:
	 *               array(
	 *                   "{roleName}" => serialised role,
	 *                   ...
	 *               )
	 *               The serialised role is the output of the RoleInterface::serialise() method.
	 */
	public function serialise()
	{
		$output = [];

		/**
		 * @var Role $role
		 */
		foreach ($this->roles as $role) {
			$output[$role->getName()] = $role->serialise();
		}

		return $output;
	}
}
